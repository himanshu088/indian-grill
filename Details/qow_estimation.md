Screens - 
1. Login
2. Register
3. Treatments
4. Shop
5. Product detail
6. About
7. About modal page (from menu)
8. Splash Screen

Total Screens 11

Functionality : 

1. Login via email password - 5000
2. Register user - 5000
3. Social Login (Facebook, twitter, instagram) - 10000
4. Treatments listing - 8000
5. Whatsapp integration - 3000
6. Email sending via firebase - 4000
7. Calling feature - 3000
8. Shop products listing - 5000

43000 + 18000 = 61000

change splash screen
text align at signup (done)
input text capitalize (done)
required with * (done)
gender selection (done)
signup via social media font change
buttons on login and regiter (done)
write text at register screen
whatsapp icon color and size with <hr> (done)
calling fab icon on home screen (done)

### Payments

- *1st Payment* - 7000 INR Jan 4, 2020
- *2nd Payment* - 10000 INR Jan 15, 2020