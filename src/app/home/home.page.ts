import { Component, OnInit } from '@angular/core';
import {
  InAppBrowser,
  InAppBrowserOptions,
} from '@ionic-native/in-app-browser/ngx';

// import { Plugins } from '@capacitor/core';
import { LoadingController } from '@ionic/angular';
// const { App } = Plugins;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  iabOptions: InAppBrowserOptions = {
    location: 'no',
    usewkwebview: 'yes',
    footer: 'no',
    toolbar: 'no',
    zoom: 'no',
  };

  constructor(
    private iab: InAppBrowser,
    private loadingCtrl: LoadingController
  ) {
    setTimeout(() => this.inAppB(), 5000);
  }

  ngOnInit() {
    this.presentLoading();
  }

  async inAppB() {
    const browser = this.iab.create(
      'https://www.indian-grill.com/',
      '_blank',
      this.iabOptions
    );
    browser.on('exit').subscribe((event) => {
      console.log('IAB Event:', event);
      if (event.type === 'exit') {
        // App.exitApp();
        // navigator['app'].exitApp();
      }
    });
    browser.show();
  }

  async presentLoading() {
    const loading = await this.loadingCtrl.create({
      message: 'Loading...',
      duration: 5000,
    });
    loading.present();
  }
}
